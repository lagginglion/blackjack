﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack.model
{
    class Dealer : Player
    {
        private Deck m_deck = null;
        private const int g_maxScore = 21;

        private rules.INewGameStrategy m_newGameRule;
        private rules.IHitStrategy m_hitRule;
        private rules.IWinGameStrategy m_winGameRule;

        private List<IGameObserver> m_listeners = new List<IGameObserver>();

        public Dealer(rules.RulesFactory a_rulesFactory)
        {
            m_newGameRule = a_rulesFactory.GetNewGameRule();
            m_hitRule = a_rulesFactory.GetHitRule();
            m_winGameRule = a_rulesFactory.GetWinGameRule();
        }

        public bool NewGame(Player a_player)
        {
            if (m_deck == null || IsGameOver())
            {
                m_deck = new Deck();
                ClearHand();
                a_player.ClearHand();
                return m_newGameRule.NewGame(m_deck, this, a_player);   
            }
            return false;
        }

        public void RegisterListener(IGameObserver a_listener)
        {
            m_listeners.Add(a_listener);
        }

        public bool Hit(Player a_player)
        {
            if (m_deck != null && a_player.CalcScore() < g_maxScore && !IsGameOver())
            {
                DealCardFromDeck(a_player);

                return true;
            }
            return false;
        }

        public bool IsDealerWinner(Player a_player)
        {
            return m_winGameRule.IsDealerWinner(this, a_player, g_maxScore);
        }

        public bool IsGameOver()
        {
            if (m_deck != null && /*CalcScore() >= g_hitLimit*/ m_hitRule.DoHit(this) != true)
            {
                return true;
            }
            return false;
        }

        public bool Stand()
        {
            if (m_deck != null) 
            {
                ShowHand();

                while (m_hitRule.DoHit(this))
                {
                    DealCardFromDeck(this);
                }
            }

            return true;

        }

        public void DealCardFromDeck(Player a_player, bool showCard = true)
        {
            model.Card c = m_deck.GetCard();
            c.Show(showCard);
            a_player.DealCard(c);

            // Notify
            foreach(IGameObserver o in m_listeners)
            {
                o.OnCardDealt(a_player, c);
            }
        }
    }
}
