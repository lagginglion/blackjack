﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack.model
{
    interface IGameObserver
    {
        void OnCardDealt(Player a_player, Card c);
    }
}
