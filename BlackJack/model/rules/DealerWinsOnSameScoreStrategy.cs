﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack.model.rules
{
    class DealerWinsOnSameScoreStrategy : IWinGameStrategy
    {
        public bool IsDealerWinner(model.Player a_dealer, model.Player a_player, int a_maxScore)
        {
            if (a_player.CalcScore() > a_maxScore)
            {
                return true;
            }
            else if (a_dealer.CalcScore() > a_maxScore)
            {
                return false;
            }
            // Dealer wins on same score or more
            return a_dealer.CalcScore() >= a_player.CalcScore();
        }
    }
}