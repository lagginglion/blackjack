﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack.model.rules
{
    class AmericanNewGameStrategy : INewGameStrategy
    {
        // TODO: Does not need a_deck
        public bool NewGame(Deck a_deck, Dealer a_dealer, Player a_player)
        {
            a_dealer.DealCardFromDeck(a_player);

            a_dealer.DealCardFromDeck(a_dealer);

            a_dealer.DealCardFromDeck(a_player);

            a_dealer.DealCardFromDeck(a_dealer, false);

            return true;
        }
    }
}
