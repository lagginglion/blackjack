﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack.model.rules
{
    class Soft17HitStrategy : IHitStrategy
    {
        private const int g_hitLimit = 17;

        public bool DoHit(model.Player a_dealer)
        {
            int score = a_dealer.CalcScore();

            // Check for soft 17
            if (score == g_hitLimit)
            {
                // Get the hand
                List<Card> hand = (List<Card>)a_dealer.GetHand();

                // Add a value 10-card
                hand.Add(new Card(Card.Color.Clubs, Card.Value.King));

                // Soft 17!
                if (a_dealer.CalcScore(hand) == 17)
                {
                    return true;
                }
               
            }

            return score < g_hitLimit;
        }
    }
}
