﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJack.model.rules
{
    class PlayerWinsOnSameScoreStrategy : IWinGameStrategy
    {
        public bool IsDealerWinner(model.Player a_dealer, model.Player a_player, int a_maxScore)
        {
            if (a_player.CalcScore() > a_maxScore)
            {
                return true;
            }
            else if (a_dealer.CalcScore() > a_maxScore)
            {
                return false;
            }
            // Dealer must have a higher score
            return a_dealer.CalcScore() > a_player.CalcScore();
        }
    }
}
